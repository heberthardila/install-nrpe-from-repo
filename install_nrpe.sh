#El siguiente script instalar el agente NRPE sobre el host GNU/Linux desde los fuentes de cada distro. 
#Realizado por: 
#Heberth Ardila
#DMCI.co
#heberth.ardila@dmci.co - heberthardila@gmail.com


#Paquetes por SO
#Debian
pack_debian="nagios-nrpe-server nagios-nrpe-plugin nagios-plugins-contrib"
#RH
pack_rh="wget nrpe nagios-plugins-all nagios-plugins-nrpe"
#Suse
pack_suse="suse"
#Rutas quemadas
#Para todos los SO
tmp_checks="/tmp/checks_base.txt"
config_nagios="/etc/nagios/"

#Debian
deb_nagios_lib="/usr/lib/nagios/plugins/"

#RH
rh_nagios_lib="/usr/lib64/nagios/plugins/"
#SuSe
suse_nagios_lib=""


#detectar sistema. 
version_linux() {
  if [ -f "/etc/debian_version" ]; then
    distro="debian"
  elif [ -f "/etc/redhat-release" ]; then
    distro="redhat"
  elif [ -f "/etc/SuSE-release" ]; then
    distro="suse"
  else
    distro="unknown"
  fi
echo "Distro identificada: $distro"
sleep 2
}

#Funcion que crea en el directorio temporal la lista de los checks a bajar
down_checks_base()
{
cat <<EOF >> ${tmp_checks}
https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/check-base/check_users_ip.pl
https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/check-base/check_await.sh
https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/check-base/check_connss.py
https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/check-base/check_cpu.sh
https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/check-base/check_iops.sh
https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/check-base/check_mem.py
https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/check-base/check_mem.sh
https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/check-base/check_netint.pl
EOF
echo "Se crea archivo temporal: ${tmp_checks}"
sleep 2
}

debian() {
echo " Ejecutando instalacion en Debian"
#Actualizar sistema
apt update &&
#Instalar paquetes
apt install -y  ${pack_debian} &&
#Configurar nrpe, se copia el archivo de configuracion. 
mv ${config_nagios}/nrpe.cfg ${config_nagios}/nrpe.cfg-backup 
wget https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/conf-base/debian/nrpe.cfg -O ${config_nagios}/nrpe.cfg &&
#Copiando archivo de configuracion de checks base. 
echo "Descargando archivo de configuracion para los check base"
wget https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/conf-base/debian/check_base.cfg -O ${config_nagios}/nrpe.d/check_base.cfg  && chown nagios:nagios ${config_nagios}/nrpe.d/check_base.cfg &&
#Descarga de checks
down_checks_base
#Copiar los checks al directorio other
mkdir ${deb_nagios_lib}/other
wget -P ${deb_nagios_lib}/other -i /tmp/checks_base.txt &&
chown -R nagios:nagios ${deb_nagios_lib}/other/ && chmod -R +x ${deb_nagios_lib}/other/
rm -rf ${tmp_checks}
#Reiniciando servicios. 
service nagios-nrpe-server restart &&
#Agregar IP del server Nagios
echo "Es necesario agregar la IP del servidor Nagios desde el cual se realizaran las consultas..."
echo "Configurela sobre la variable allowed_hosts en ${config_nagios}/nrpe.cfg"

}

redhat() {
echo "Ejecutando instalacion en RedHat"
yum check-update &&
sleep 1
#yum update -y &&
#Se instalan los repositorios epel para proceder con nrpe
yum install epel-release -y &&
sleep 1
yum check-update &&
sleep 1
#Instalar paquetes nrpe
yum install ${pack_rh} -y &&
sleep 1
#Configurar nrpe, se copia el archivo de configuracion. 
cp ${config_nagios}/nrpe.cfg ${config_nagios}/nrpe.cfg-backup &&
> ${config_nagios}/nrpe.cfg
wget https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/conf-base/redhat/nrpe.cfg -O ${config_nagios}/nrpe.cfg &&
#Copiando archivo de configuracion de checks base. 
echo "Descargando archivo de configuracion para los check base" &&
wget https://gitlab.com/heberthardila/install-nrpe-from-repo/raw/master/conf-base/redhat/check_base.cfg -O /etc/nrpe.d/check_base.cfg  && chown nagios:nagios /etc/nrpe.d/check_base.cfg &&
#Descarga de checks
down_checks_base
#Copiar los checks al directorio other
mkdir ${rh_nagios_lib}/other
wget -P ${rh_nagios_lib}/other -i ${tmp_checks} &&
chown -R nagios:nagios ${rh_nagios_lib}/other/ && chmod -R +x ${rh_nagios_lib}/other/ &&
#Reiniciando servicios. 
rm -rf ${tmp_checks}
systemctl restart nrpe &&
#Agregar IP del server Nagios
echo "Es necesario agregar la IP del servidor Nagios desde el cual se realizaran las consultas..." &&
echo "Configurela sobre la variable allowed_hosts en ${config_nagios}/nrpe.cfg"

}

suse() {
echo "Ejecutando instalacion en SuSe"
}

unknown() {
echo "Distro no reconocida... :/"
}

start() {
  version_linux
  $distro
}

start

